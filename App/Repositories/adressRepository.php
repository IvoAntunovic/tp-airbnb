<?php

/**
 * Classe AdressRepository
 */

namespace App\Repositories;

use App\Models\Adress;

class AdressRepository extends Repository 
{
    public function getTable(): string
    {
       return 'adresses';
    }

    public function findAll(): array
    {
        return $this->readAll(Adress::class);
    }

    public function findById( int $id ): ?Adress
    {
        return $this->readById(Adress::class, $id);
    }
}
