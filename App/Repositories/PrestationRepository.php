<?php

/**
 * Classe PrestationRepository
 */

namespace App\Repositories;

use App\Models\Prestation;

class PrestationRepository extends Repository 
{
    public function getTable(): string
    {
       return 'prestations';
    }

    public function findAll(): array
    {
        return $this->readAll(Prestation::class);
    }

    public function findById( int $id ): ?Prestation
    {
        return $this->readById(Prestation::class, $id);
    }
}
