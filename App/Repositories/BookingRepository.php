<?php

/**
 * Classe AdressRepository
 */

namespace App\Repositories;

use App\Models\Booking;

class BookingRepository extends Repository 
{
    public function getTable(): string
    {
       return 'bookings';
    }

    public function findAll(): array
    {
        return $this->readAll(Booking::class);
    }

    public function findById( int $id ): ?Booking
    {
        return $this->readById(Booking::class, $id);
    }
}
