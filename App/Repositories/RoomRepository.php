<?php

namespace App\Repositories;

use Exception;

use App\Models\Room;

class RoomRepository extends Repository
{
	public function getTable(): string
	{
		return 'rooms';
	}

	// cRud - READ - Toutes les voitures en données brutes de la table "cars"
	public function findAll( array $query_addons = [], array $addon_data = [] ): array
	{
		return $this->readAll( Room::class, $query_addons, $addon_data );
	}

	// cRud - READ - Une voiture en données brutes de la table "cars"
	public function findById( int $id ): ?Room
	{
		return $this->readById( Room::class, $id );
	}
}