<?php

/**
 * Classe EquipmentRepository
 */

namespace App\Repositories;

use App\Models\Equipment;

class EquipmentRepository extends Repository 
{
    public function getTable(): string
    {
       return 'equipments';
    }

    public function findAll(): array
    {
        return $this->readAll(Equipment::class);
    }

    public function findById( int $id ): ?Equipment
    {
        return $this->readById(Equipment::class, $id);
    }
}
