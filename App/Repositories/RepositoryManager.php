<?php

namespace App\Repositories;

class RepositoryManager
{
	private static ?self $instance = null;

	private UserRepository $userRepository;
	public function getUserRepository(): UserRepository { return $this->userRepository; }

	private AdressRepository $adressRepository;
	public function getAdressRepository(): AdressRepository { return $this->adressRepository; }

	
	private BookingRepository $bookingRepository;
	public function getBookingRepository(): BookingRepository { return $this->bookingRepository; }

	private EquipmentRepository $equipmentRepository;
	public function getEquipmentRepository(): EquipmentRepository { return $this->equipmentRepository; }

	private PrestationRepository $prestationRepository;
	public function getPrestationRepository(): PrestationRepository { return $this->prestationRepository; }

	private RoomRepository $roomRepository;
	public function getRoomRepository(): RoomRepository { return $this->roomRepository; }

	public static function getRm(): self
	{
		if( is_null(self::$instance) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	private function __construct()
	{
		$this->userRepository = new UserRepository();
		$this->adressRepository = new AdressRepository();
		$this->bookingRepository = new BookingRepository();
		$this->equipmentRepository = new EquipmentRepository();
		$this->prestationRepository = new PrestationRepository();
		$this->roomRepository = new RoomRepository();
	}

	private function __clone() { }
	private function __wakeup() { }
}