

<?php
var_dump($_SESSION['USER']->id);
?>

<?php if( empty( $rooms ) ): ?>
	<div>Aucune chambres pour l'instant :'(</div>
<?php else: ?>
	<ul>
		<?php foreach( $rooms as $rooms ): ?>
			<li>
				<h2>
					<a href="/rooms/<?php echo $room->id ?>">
						<?php echo $room->id ?>
					</a>
				</h2>
				<p><?php echo $room->surface ?></p>
				<p><?php echo str_replace( '.', ',', $room->price ) ?> €</p>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>