<?php

namespace App\Controllers;

use Laminas\Diactoros\ServerRequest;

use App\View;
use App\Repositories\RepositoryManager;

class RoomController
{
	/*
	 * Actions
	 */

	public function homeList(): void
	{
		$view = new View( 'rooms\home_list' );
		$query_addons = [];
		$html_title = '';


		// Si le user est un annonceur, on fabrique une requête qui charge ses propriétés
		if($_SESSION['USER']->user_type === 2) {
			// User (JOIN)
			array_push( $query_addons, 'WHERE user_id=' . $_SESSION['USER']->id);

			// On change le titre de l'entête pour personnaliser la page owner 
            $html_title = 'Vos propriétés';
		}
		// Sinon on charge simplement la liste des annonces du site.
		else {

		}

			// Mileage (clause WHERE)
			// La première condition du WHERE "1=1" donne toujours true
			// ce qui permet de préfixer toutes les conditions suivantes avec "AND" ou "OR"
			// sans se préoccuper de savoir si on doit mettre "WHERE" ou non
			// array_push( $query_addons, 'WHERE 1=1' );

				// array_push( $query_addons, 'AND mileage <= :mileage');
				// $addon_data['mileage'] = $mileage;
			



		$view_data = [
			'html_title' => $html_title,
			'rooms' => RepositoryManager::getRm()->getRoomRepository()->findAll( $query_addons )
		];

		$view->render( $view_data );
	}



	// Page détail d'une chambre disponible
	public function rooms(): void 
	{
		$view = new View( 'rooms\rooms' );

		$view->render([
			'html_title' => 'Page détail de la chambre'
		]);
	}

	// Page détail d'une chambre disponible
	public function bookings(): void 
	{
		$view = new View( 'rooms\bookings' );

		$view->render([
			'html_title' => 'Page détail de la chambre réservée'
		]);
	}




	 // Page d'ajout d'une chambre
	 public function addRoom(): void 
	 {
		$view = new View('rooms\add_room');

		$view->render([
			'html_title' => 'Ajoutez une chambre'
		]);
	 }


	public function show( int $id ): void
	{
		$room = RepositoryManager::getRm()->getRoomRepository()->findById( $id );

		// Si la voiture n'existe pas on lance la page 404
		if( is_null( $room ) ) {
			View::render404();
			return;
		}

		$view = new View( 'rooms\detail_room' );
		$view->render([
			'html_title' => 'Chambre N°' . $room->id,
			'room_description' => $room,
			'room' => $room
		]);
	}



	public function showBySlug( string $slug ): void
	{
		echo 'Voici la chambre: '. $slug;
	}



	public function create(): void
	{

	}
}