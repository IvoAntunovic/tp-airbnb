<?php

/**
 * Classe Booking
 */

namespace app\Models;

use App\Repositories\RepositoryManager;

class Booking extends Model
{
  public string $start_date;
  public string $end_date;

 // Propriétés provenant de tables étrangères
  public int $id_user;
  public int $id_room;

  protected User $_user;
  protected function user(): User 
  {
    $this->_user = RepositoryManager::getRm()->getUserRepository()->findById( $this->id_user );
    return $this->_user;
  }

  protected Room $_room;
  protected function room(): Room 
  {
    $this->_room = RepositoryManager::getRm()->getRoomRepository()->findById( $this->id_room );
    return $this->_room;
  }


}