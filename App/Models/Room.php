<?php

/**
 * Classe Room
 */

 namespace App\Models;

use App\Repositories\RepositoryManager;

class Room extends Model
 {

    public const FREE = 1;
    public const RESERVED = 2;

     public float $price;
     public int $room_type;
     public int $surface;
     public string $description;
     public int $sleeping;
     public int $room_status;

     public int $id_adress;
     public int $id_user;

     protected Adress $_adress;
     protected function adress(): Adress 
     {
         $this->_adress = RepositoryManager::getRm()->getAdressRepository()->findById($this->id_adress);
         return $this->_adress;
     }

     protected User $_user;
     protected function user(): User 
     {
         $this->_user = RepositoryManager::getRm()->getUserRepository()->findById($this->id_user);
         return $this->_user;
     }



     public function getRoomStatus(): string
     {
         $room_status = 0;
     
         switch( $this->room_status ) {
             case self::FREE:
     
                 $user_type = 'LIBRE';
                 break;
     
             case self::RESERVED:
     
                 $user_type = 'RÉSERVÉE';
                 break;
         }
     
         return $user_type;
     }


 }