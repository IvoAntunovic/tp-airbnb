<?php

/**
 * Classe Prestation
 */

namespace App\Models;

use App\Repositories\RepositoryManager;

 class Prestation extends Model
 {
     public int $id_room;
     public int $id_equipment;

     	// Propriétés issues des colonnes de la table "restations"
	public int $engine_size_id;

	// Propriétés de modélisation complète
	protected Room $_room;
	protected function room(): Room
	{
		if( !isset( $this->_room )) {
			$this->_room = RepositoryManager::getRm()->getRoomRepository()->findById( $this->id_room );
		}

		return $this->_room;
	}

	protected Equipment $_equipment;
	protected function equipment(): Equipment
	{
		if( !isset( $this->_equipment ) ) {
			$this->_equipment = RepositoryManager::getRm()->getEquipmentRepository()->findById( $this->id_equipment );
		}

		return $this->_equipment;
	}


 }
