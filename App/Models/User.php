<?php

/**
 * Class User
 */

namespace App\Models;

class User extends Model
{
    public const USER = 1;
    public const OWNER = 2;
    
    
    public string $pseudo;
    public string $email;
    public string $password;
    public int $user_type;
    

    public function getRoleName(): string
    {
        $user_type = '';
    
        switch( $this->user_type ) {
            case self::USER:
    
                $user_type = 'USER';
                break;
    
            case self::OWNER:
    
                $user_type = 'OWNER';
                break;
    
            default:
                $user_type = 'HACKER';
                break;
        }
    
        return $user_type;
    }



    public static function hashPassword( string $password ): string
	{
		return  HASH_ONE.$password.HASH_ZERO;
	}





	public static function fromSession(): ?self
	{
    	if( ! self::isAuth() ) {
    		return null;
		}

    	return $_SESSION[ 'USER' ];
	}


	public static function isAuth(): bool
	{
		return isset( $_SESSION[ 'USER' ] );
	}




	public static function isAdmin(): bool
	{
		if( ! self::isAuth() ) {
			return false;
		}

		$user = self::fromSession();

		return $user->role === self::USER || $user->role === self::OWNER;
	}




}