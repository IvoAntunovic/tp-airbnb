<?php


/**
 * Classe Adress
 */

namespace App\Models;

class Adress extends Model
{
    public string $country;
    public string $town;
}